package com.emailnotify.clientdetails;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.emailnotify.emailnotify.*;

@Repository
public class ClientJdbcDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<ClientDetails> findAll(){
		return (List<ClientDetails>) jdbcTemplate.query("select clientName from clienttable where clntId='01';",
					new BeanPropertyRowMapper<ClientDetails>(ClientDetails.class));
	}
	public List<ClientDetails> selectList(){
		return (List<ClientDetails>) jdbcTemplate.query("select clientName from clienttable where clntId='02';",
					new BeanPropertyRowMapper<ClientDetails>(ClientDetails.class));
	}
	public List<ClientEmail> selectList1(){
		return (List<ClientEmail>) jdbcTemplate.query("select email from clienttable where clntId='03';",
					new BeanPropertyRowMapper<ClientEmail>(ClientEmail.class));
	}
	public List<ClientEmail> selectList2(){
		return (List<ClientEmail>) jdbcTemplate.query("select email from clienttable where clntId='04';",
					new BeanPropertyRowMapper<ClientEmail>(ClientEmail.class));
	}	
}


