package com.emailnotify.emailnotify;
import java.io.ByteArrayOutputStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;

import com.emailnotify.clientdetails.ClientJdbcDao;

import antlr.collections.List;

@ConfigurationProperties(prefix = "spring.data.email")
@SpringBootApplication(scanBasePackages = {"com.emailnotify.clientdetails", "com.emailnotify.emailnotify"})
@Repository
public class EmailnotifyApplication implements CommandLineRunner{ 
	@Autowired
    private JavaMailSender javaMailSender;
	String subject;
    String content;
    String regards;
    String callbacklink;
    String clientname;
    String email;
    Date datesub = new Date(System.currentTimeMillis());
    
    
   Map<Tokens, Object> mapvalues = new HashMap<Tokens, Object>();
    
    public String getcallbacklink() {
        return callbacklink;
    }

    public void setcallbacklink(String callbacklink) {
        this.callbacklink = callbacklink;
    }
    public String getsubject() {
        return subject;
    }

    public void setsubject(String subject) {
        this.subject = subject;
    }
    public String getcontent() {
        return content;
    }

    public void setcontent(String content) {
        this.content = content;
    }
    public String getregards() {
        return regards;
    }

    public void setregards(String regards) {
        this.regards = regards;
    }
    public String getclientname() {
        return clientname;
    }

    public void setclientname(String clientname) {
        this.clientname = clientname;
    }
    
    Date fromDate = new Date(System.currentTimeMillis());
	Date toDate = new Date(System.currentTimeMillis());
	
	public static void main(String[] args) {
		SpringApplication.run(EmailnotifyApplication.class, args);
	}
		    
	@Autowired
	ClientJdbcDao clientJdbcDao;
	
	
	@Override
	public void run(String... args) throws Exception {
		
		for(Tokens tokenName : Tokens.values()) {
	    	
			switch (tokenName) {
			case FROM_DT:
				java.util.Date fromdate = new Date(System.currentTimeMillis());  
		        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd ");  
		        String strFrom = dateFormat1.format(fromdate);  
				ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
				baos1.write(strFrom.getBytes("UTF-8"));
				byte[] fromdata = baos1.toByteArray();
				
				mapvalues.put(tokenName, fromdate);
				subject = subject.replaceAll(tokenName.getTokenName(), mapvalues.get(tokenName).toString());
				callbacklink = callbacklink.replaceAll(tokenName.getTokenName(), mapvalues.get(tokenName).toString());
				break;
			case TO_DT:
				java.util.Date todate = new Date(System.currentTimeMillis());  
		        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd ");  
		        String strTo = dateFormat2.format(todate);  
				ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
				baos2.write(strTo.getBytes("UTF-8"));
				byte[] todata = baos2.toByteArray();
				mapvalues.put(tokenName, todate);
				callbacklink = callbacklink.replaceAll(tokenName.getTokenName(), mapvalues.get(tokenName).toString());
				
				break;
			case CALLBACKLINK:
				mapvalues.put(tokenName, callbacklink);
				break;
			case CONTENT:
				mapvalues.put(tokenName, content);
				callbacklink = callbacklink.replaceAll(tokenName.getTokenName(), mapvalues.get(tokenName).toString());
				break;
			case DATE_SUB:
				mapvalues.put(tokenName, datesub);
				subject = subject.replaceAll(tokenName.getTokenName(), mapvalues.get(tokenName).toString());
				break;
			default:
				break;
			}
	
    	}
		
		 System.out.println("Sending Email...");
		
	    sendEmail(); 
	   
	     System.out.println("Done");		
	}
	void sendEmail() {
		java.util.List<ClientDetails> clientName = clientJdbcDao.findAll();
		String str=clientName.toString();	
	
		java.util.List<ClientDetails> clientName1 = clientJdbcDao.selectList();
		String str1=clientName1.toString(); 
		
		java.util.List<ClientEmail> email = clientJdbcDao.selectList1();
		String emailstr=email.toString(); 
		
		java.util.List<ClientEmail> email1 = clientJdbcDao.selectList2();
		String emailstr1=email1.toString(); 
		
		SimpleMailMessage msg = new SimpleMailMessage();
		
		SimpleMailMessage msg1 = new SimpleMailMessage();
		
		System.out.println(emailstr);
		System.out.println(emailstr1);
		
		//msg.setTo(ClientEmail.getemail());
		msg.setTo(ClientEmail.getemail());
        msg1.setTo("sandhya.ruby96@gmail.com");
        
        msg.setSubject(subject);
        msg1.setSubject(subject);
        
        msg.setText(clientname +str +"\n" +content+ "\n" +callbacklink+ "\n" +regards); 
        msg1.setText(clientname +str1 +"\n" +content+ "\n" +callbacklink+ "\n" +regards); 
        
        javaMailSender.send(msg);
        javaMailSender.send(msg1);

    }
	
}
