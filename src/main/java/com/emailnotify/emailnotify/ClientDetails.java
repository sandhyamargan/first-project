package com.emailnotify.emailnotify;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class ClientDetails {
	@Id
	@GeneratedValue
	String clientName;
	
	public ClientDetails() {}
	
	public ClientDetails(String clientName
			) {
		super();
		
		this.clientName= clientName;
		
		
	}

	public String getclientName() {
		return clientName;
	}

	public void setclientName(String clientName) {
		this.clientName = clientName;
	}

	@Override
	public String toString() {
		return  "" +clientName+ "";
		
		//return getemail();
		
	}
	

}
