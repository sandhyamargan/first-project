package com.emailnotify.emailnotify;

public enum Tokens {
	
	FROM_DT("<fromDate>"), TO_DT("<toDate>"), CALLBACKLINK("<callbacklink>"), CONTENT("<content>"), DATE_SUB("<datesub>");
	
	
	String tokenName;
	
	private Tokens(String tokenName){
		this.tokenName=tokenName;
	}

	public String getTokenName() {
        return tokenName;
    }
	
}

